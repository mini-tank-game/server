using System;
using System.Net;
using System.Net.Sockets;
using System.Linq;
using System.Collections.Generic;


namespace Server
{
    class Server
    {
        public static int counterPlayer = -1;

        private bool running = false;
        private UdpClient socket;
        private IPEndPoint ipEndPoint;
        public IPEndPoint currentEndPoint;
        private HandlePackets handlePackets;
        public static Dictionary<IPEndPoint, Player> playerList = new Dictionary<IPEndPoint, Player>();

        Byte[] receivedByte;
        bool handleData = false;

        public Server(int port)
        {
            InitServer(port);
            running = true;
        }

        public bool IsRunning()
        {
            return running;
        }

        public void Update()
        {
            if (handleData == true)
            {
                handlePackets.HandleData(receivedByte);
                handleData = false;

                //Receive again
                socket.BeginReceive(OnReceive, null);
            }
        }


        private void InitServer(int port)
        {
            //Init server endpoint
            ipEndPoint = new IPEndPoint(IPAddress.Any, port);
            socket = new UdpClient(ipEndPoint);
            handlePackets = new HandlePackets(this);

            Console.WriteLine("Server up!...");

            //Start accept new client
            socket.BeginReceive(OnReceive, null);
        }

        public void Send(IPEndPoint endPoint, byte[] data)
        {
            socket.SendAsync(data, data.Length, endPoint);
        }

        public void SendAll(byte[] data)
        {
            foreach (KeyValuePair<IPEndPoint, Player> e in playerList)
            {
                try
                {
                    Send(e.Key, data);
                }
                catch { }
            }
        }


        private void OnReceive(IAsyncResult result)
        {
            receivedByte = socket.EndReceive(result, ref currentEndPoint);
            handleData = true;
        }

        public void SendAllExcept(byte[] data, string username)
        {
            foreach (KeyValuePair<IPEndPoint, Player> e in playerList)
            {
                try
                {
                    if (e.Value.username != username)
                        Send(e.Key, data);
                }
                catch { }
            }
        }


    }
}