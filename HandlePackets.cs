using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;

namespace Server
{
    public enum Packets
    {
        S_JOIN = 1,
        S_TRANSFORM,
        S_DISCONNECT,
        S_FIRE
    };


    class HandlePackets
    {
        public static ByteBuffer playerBuffer;
        private static long pLength;
        private delegate void Packet_(byte[] data);
        private static Dictionary<long, Packet_> packets;
        private Server server;

        public HandlePackets(Server server)
        {
            this.server = server;
            InitializePackets();
        }

        private void InitializePackets()
        {
            packets = new Dictionary<long, Packet_>();
            packets.Add((long)Packets.S_JOIN, PACKET_JOIN);
            packets.Add((long)Packets.S_TRANSFORM, PACKET_TRANSFORM);
            packets.Add((long)Packets.S_DISCONNECT, PACKET_DC);
            packets.Add((long)Packets.S_FIRE, PACKET_FIRE);
        }

        public void HandleData(byte[] data)
        {
            byte[] Buffer;
            Buffer = (byte[])data.Clone();

            if (playerBuffer == null)
            {
                playerBuffer = new ByteBuffer();
            }

            playerBuffer.WriteBytes(Buffer);

            if (playerBuffer.Count() == 0)
            {
                playerBuffer.Clear();
                return;
            }

            if (playerBuffer.Length() >= 8)
            {
                pLength = playerBuffer.ReadLong(false);

                if (pLength <= 0)
                {
                    playerBuffer.Clear();
                    return;
                }
            }

            playerBuffer.ReadLong();

            HandleDataPackets(data);

            playerBuffer.Clear();
            return;
        }

        public void HandleDataPackets(byte[] data)
        {
            long packetIdentifier;
            ByteBuffer buffer;
            Packet_ packet;

            buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            packetIdentifier = buffer.ReadLong();

            buffer.Dispose();

            if (packets.TryGetValue(packetIdentifier, out packet))
            {
                packet.Invoke(data);
            }

        }

        private void PACKET_JOIN(byte[] data)
        {
            Console.WriteLine("Packet Join ");

            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadLong();

            string username = buffer.ReadString();
            Server.counterPlayer++;

            Player p = new Player(Server.counterPlayer, username, 0f, Server.counterPlayer * 10f, 0f);
            buffer = new ByteBuffer();
            buffer.WriteLong((long)Packets.S_JOIN);
            buffer.WriteInteger(p.id);
            buffer.WriteString(p.username);
            buffer.WriteFloat(p.zRotation);
            buffer.WriteFloat(p.xPosition);
            buffer.WriteFloat(p.yPosition);

            // Send new player to current all player
            server.SendAll(buffer.ToArray());

            // Add new player to list
            Server.playerList.Add(server.currentEndPoint, p);

            // Send current player to new player
            foreach (KeyValuePair<IPEndPoint, Player> e in Server.playerList)
            {
                Player player = e.Value;

                buffer = new ByteBuffer();
                buffer.WriteLong((long)Packets.S_JOIN);
                buffer.WriteInteger(player.id);
                buffer.WriteString(player.username);
                buffer.WriteFloat(player.zRotation);
                buffer.WriteFloat(player.xPosition);
                buffer.WriteFloat(player.yPosition);

                server.Send(server.currentEndPoint, buffer.ToArray());
            }
        }

        private void PACKET_TRANSFORM(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            string username = Server.playerList[server.currentEndPoint].username;
            buffer.WriteString(username);

            // Send position to all player
            server.SendAllExcept(buffer.ToArray(), username);
        }

        private void PACKET_FIRE(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);

            string username = Server.playerList[server.currentEndPoint].username;
            buffer.WriteString(username);

            // Send position to all player
            server.SendAllExcept(buffer.ToArray(), username);
        }

        private void PACKET_DC(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteLong((long)Packets.S_DISCONNECT);

            string username = Server.playerList[server.currentEndPoint].username;
            buffer.WriteString(username);

            // Send position to all player
            server.SendAllExcept(buffer.ToArray(), username);
        }
    }
}