﻿using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server(1000);

            while (server.IsRunning())
            {
                //Must fps implementation to limit looping wait sleep
                server.Update();
            }
        }
    }
}
