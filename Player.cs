using System;
using System.Net;
using System.Net.Sockets;


namespace Server
{
    class Player
    {
        public int id;
        public string username;

        // Player stats
        public float zRotation;
        public float xPosition;
        public float yPosition;

        public Player(int id, string username, float z, float x, float y)
        {
            this.id = id;
            this.username = username;
            this.zRotation = z;
            this.xPosition = x;
            this.yPosition = y;
        }
    }
}